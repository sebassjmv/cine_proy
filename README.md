# cine_proy



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sebassjmv/cine_proy.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sebassjmv/cine_proy/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

### CIN3S

El proyecto que nosotros realizaremos será ‘CIN3S’, el cuál consiste en la página web de la reserva de las diferentes películas que se ofrecerán en dicha página. Para cada película se tendrá en cuenta la sala, el tipo de proyección, el doblaje de la película, los horarios y las sillas que se escogen, la variación de estas afectarán en el precio final de la reserva, la comida ya será por aparte de la reserva, contará con días de promoción, es decir, depende del dia en que se reserve la función, puede costar más o menos. La página contará con diferentes métodos de pago para facilitar la compra de este.




### Guía de uso JavaScript

Link: [https://github.com/airbnb/javascript]

### Guía de React 

Link: [https://reactjs.org/docs/getting-started.html]

### pruebas
https://docs.google.com/spreadsheets/d/1em_6x1qwSnvIpMfAaNhBn4S7k3uUur8R/edit?usp=sharing&ouid=109129324657068924477&rtpof=true&sd=true

### Integrantes
Sebastián Murillo
Santiago Escobar
Santiago Posada