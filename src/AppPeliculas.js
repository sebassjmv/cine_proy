import { useState, useEffect} from "react";
import { Routes, Route, Link } from "react-router-dom";
import ListaPeliculas from "./Componentes/lista-peliculas/lista-peliculas";
import Acerca from "./Paginas/Acerca";
import DetallesPeliculas from "./Paginas/DetallesPeliculas";


const App = () =>{
    const [peliculas, setPeliculas] = useState([]);
    const [mostrarPeliculas, setMostrarPeliculas] = useState(false);
    const [buscarInput, setBuscarInput] = useState('');

    useEffect(()=>{
      fetch(
               "http://localhost:8000/peliculas"
            )
            .then((response) => response.json())
            .then((apiPeliculas)=>{
             setPeliculas(apiPeliculas);
             setMostrarPeliculas(true);
              });
    },[]);

   const  buscarPeliculasHandler = (evento) =>{
         const buscar  = evento.target.value.toLocaleLowerCase();
         setBuscarInput(buscar);
           
         };

    const filtrarPeliculas = peliculas.filter((pelicula) => {
      return pelicula.Title.toLocaleLowerCase().includes(buscarInput);
    });

    let renderPeliculas = "cargando peliculas";
    
    if(mostrarPeliculas){
      renderPeliculas=(
       <ListaPeliculas pelicula={filtrarPeliculas} />
      )
    }

  return (
    <div className='max-w-6xl mx-auto bg-slate-200'>
      <div className="relative flex items-center justify-between mb-8 h-16 bg-yellow-500">
      <div className="flex items-center px-2 lg:px-0">
        <Link to={"/"} className="flex ml-2 text-2xl font-semibold">
          Cartelera
        </Link>
        <div className="hidden lg:block lg:ml-2">
          <div className="flex">
            <Link
              to={`/acerca`}
              className="ml-4 px-3 py-2 rounded-md text-sm leading-5 text-gray-800 font-semibold hover:bg-yellow-500 hover:text-white transition duration-150 ease-in-out cursor-pointer focus:outline-none focus:text-white focus:bg-gray-700"
            >
              Acerca
            </Link>
          </div>
        </div>
      </div>
      <div className="flex-1 flex justify-center px-2 lg:ml-6 lg:justify-end">
        <div className="max-w-lg w-full lg:max-w-xs">
          <label for="search" className="sr-only">
            Search{" "}
          </label>
          <form methode="get" action="#" className="relative z-50">
            <button
              type="submit"
              id="searchsubmit"
              className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none"
            >
              <svg
                className="h-5 w-5 text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
              >
                <path
                  fill-rule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </button>
            <input
              type="buscar"
              className="block w-full pl-10 pr-3 py-2 border border-transparent rounded-md leading-5 bg-yellow-200 text-gray-300 placeholder-gray-400 focus:outline-none focus:bg-white focus:text-gray-900 sm:text-sm transition duration-150 ease-in-out"
              placeholder="Buscar"
              onChange={buscarPeliculasHandler}
            />
          </form>
        </div>
      </div>
    </div>
     <h1 className='flex justify-center text-2xl font-semibold' >bienvenidos</h1>
      <div className='flex justify-center'>
      <input className='m-2 p-2 text-gray-400 rounded-mb' 
       type='buscar' placeholder='buscar peliculas' onChange={buscarPeliculasHandler}
      />
      </div>
      <Routes>
        <Route path="/" element={renderPeliculas}/>
        <Route path="acerca" element={<Acerca />}/>
        <Route path="peliculas/:id" element={<DetallesPeliculas />}/>
      </Routes>
           
    </div>
   )
   };


export default App;



