import React, {useState} from 'react'
import Silla from '../Silla/Silla';
import SillaVerde from './../../resources/img/silla_verde.png';
import SillaBlanca from './../../resources/img/silla_blanca.png';
import SillaRoja from './../../resources/img/silla_roja.png';
import Pelicula from './../../resources/img/vengadores.png';
import '../styles/Sillas.css'
//import 'bootstrap/dist/css/bootstrap.min.css';
//import { Navbar, Nav } from 'react-bootstrap';

const Sillas = () => {
    const [sillas, setSillas] = useState([
        { id: 1, isSelected: false, isReserved: false, img: SillaVerde},
        { id: 2, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 3, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 4, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 5, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 6, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 7, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 8, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 9, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 10, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 11, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 12, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 13, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 14, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 15, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 16, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 17, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 18, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 19, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 20, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 21, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 22, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 23, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 24, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 25, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 26, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 27, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 28, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 29, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 30, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 31, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 32, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 33, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 34, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 35, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 36, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 37, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 38, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 39, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 40, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 41, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 42, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 43, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 44, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 45, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 46, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 47, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 48, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 49, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 50, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 51, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 52, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 53, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 54, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 55, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 56, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 57, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 58, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 59, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 60, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 61, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 62, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 63, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 64, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 65, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 66, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 67, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 68, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 69, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 70, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 71, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 72, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 73, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 74, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 75, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 76, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 77, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 78, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 79, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 80, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 81, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 82, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 83, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 84, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 85, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 86, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 87, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 88, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 89, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 90, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 91, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 92, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 93, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 94, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 95, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 96, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 97, isSelected: false, isReserved: false, img: SillaVerde  },
        { id: 98, isSelected: false, isReserved: true, img: SillaVerde  },
    ]);

    const sillasPorFila = 14;
    const filas = [...Array(Math.ceil(sillas.length / sillasPorFila))];
    const filasDeSillas = filas.map((fila, index) => sillas.slice(index * sillasPorFila, index * sillasPorFila + sillasPorFila));

    const handleSillaClick = (id) => {
        setSillas(sillas.map(silla => {
            if (silla.id === id) {
                if (silla.isReserved == false){
                    if (silla.isSelected == true){
                        return { ...silla, isSelected: !silla.isSelected, img: SillaVerde };
                    } else {
                        return { ...silla, isSelected: !silla.isSelected, img: SillaBlanca };
                    }
                } else {
                    return { ...silla, img: SillaRoja };
                }
            } else {
              return silla;
            }
        }));
    };

    const handleComprarClick = () => {
        setSillas(
          sillas.map((silla) => {
            if (silla.isSelected) {
              return { ...silla, isSelected: false, isReserved: true , img: SillaRoja};
            } else {
              return silla;
            }
          })
        );
      };

    return (
        <div className='mainContainer'>
            <div className='contenedor-pelicula imageContainer'>
                <img className='pelicula' src={Pelicula} alt="Pelicula"/>
                <p className = "titulo-pelicula" id="titulo-pelicula">Los Vengadores Endgame</p>
            </div>
            <div className="contenedor">
                <div className='escoger'> Escoger Asientos
                <div className='contenedor-principal'>
                    <div className="contenedor-pantalla">
                        <div className="titulo-pantalla"></div>
                        <div className="pantalla">Pantalla</div>
                        <div className="contenedor-sillas">
                            {filasDeSillas.map((fila, index) => (
                                <div key={index} className="fila">
                                    {fila.map((silla) => (
                                        <Silla 
                                            key={silla.id} 
                                            img={silla.img}
                                            isSelected={silla.isSelected}
                                            isReserved={silla.isReserved}
                                            style={{ cursor: "pointer", margin: "10px" }}
                                            onClick={() => handleSillaClick(silla.id)}
                                        />
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <div className='contenedor-guia'>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: '20px' }}>
                    <p>Silla Disponible</p>
                    <img className='silla-img' src={SillaVerde} alt="SillaVerde"/>
                    <p>Silla Seleccionada</p>
                    <img className='silla-img' src={SillaBlanca} alt="SillaBlanca"/>
                    <p>Silla Reservada</p>
                    <img className='silla-img' src={SillaRoja} alt="SillaRoja"/>
                </div>
            </div>
            <footer className='sillas-seleccionadas'>Sillas seleccionadas: {sillas.filter((silla) => silla.isSelected).length}</footer>
            <div className='contenedor-boton'>
                <button className='boton-comprar' onClick={handleComprarClick}>Comprar</button>
            </div>
        </div>
    </div>
        
        
    );
}


export default Sillas;