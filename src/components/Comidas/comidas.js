import React, {useState} from 'react';
import Menu from "./Menu";
import Categories from "./Categories";
import items from "./data";
import logo from "./logo.JPG";

const allCategories = ["all", ...new Set(items.map((item) => item.category))];

const comidas = () =>{
  const [menuItems, setMenuItems] = useState(items);
  const [activeCategory, setActiveCategory] = useState("");
  const [categories, setCategories] = useState(allCategories)
  const filterItems = (category) => {
    setActiveCategory(category);
    if(category === "all"){
      setMenuItems(items)
      return;
    }
    const newItems = items.filter((item) => item.category === category);
    setMenuItems(newItems);
  }
  return (
    <main>
      <section className="menu section">
      <div className='title'>
        <img src={logo} alt="logo" className="logo"/>
        <h2>Menu list</h2>
        <div className='underLine'></div>
      </div>
      <Categories categories={categories} activeCategory={categories} filterItems={filterItems}/>
      <Menu items={menuItems}/>
      </section>
    </main>
  );
};

export default comidas;