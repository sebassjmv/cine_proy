import { Component } from "react";
import Logo from './../../resources/img/Logo.png'
import { MenuData } from "./menudata";
import './navbar.css';

class Navbar extends Component{
    state = {clicked: false};
    handleClick =()=>{
        this.setState({clicked: !this.state.clicked})
    }
    render(){
        return(
            <nav className="NavbarItems">
                <h1>
                    <img className='Logo' src={Logo} alt="Logo"/>
                </h1>
                <div className="menu-icons" onClick={this.handleClick}>
                    <i className={this.state.clicked ? "fas fa-times" : "fas fa-bars"}></i>
                </div>
                <ul className="nav-menu">
                    {MenuData.map((item, index)=>{
                        return(
                            <li key={index}>
                                <a href={item.url} className={item.cName}>
                                    <i className={item.icon}>
                                    </i>{item.title}
                                </a>
                            </li>  
                        )
                    })}
                    
                </ul>
            </nav>
        )
    }
}

export default Navbar;