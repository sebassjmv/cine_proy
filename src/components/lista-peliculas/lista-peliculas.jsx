import { Component } from "react";
import PeliculaCard from "../pelicula-card/pelicula-card";
class ListaPeliculas extends Component{
    render(){
        return <div className="grid grid-cols-3 gap-4">
        {this.props.pelicula.map((pelicula)=>{
      return <PeliculaCard pelicula= {pelicula} /> 
    })}
      </div>
    }
}

export default ListaPeliculas;