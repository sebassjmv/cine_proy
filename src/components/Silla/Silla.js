import React from 'react'

const Silla = (props) => {
  return (
    <div class = "col-lg-4">
        <div class = "text-center card-box">
            <div class = "member-card pt-2 pb-2">
                <div class = "silla">
                <img
                    src = {props.img}
                    class = "rounded-circle img-thumbnail"
                    alt = 'silla'
                    onClick={props.onClick}
                />
                </div>
            </div>
        </div>
    </div>
  )
}

export default Silla;