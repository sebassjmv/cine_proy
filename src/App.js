import React from 'react';
import './App.css'
import Sillas from './components/Sillas/sillas';
import Navbar from './components/Navbar/navbar';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Sillas/>
    </div>
  );
}

export default App;
